# JirR02 OS

## Prerequisites

Make zsh your default shell and run this script in a zsh environment.

## About this OS

This OS is built to suit my everyday use cases. The OS is highly inspired by DT and other YouTubers to make xmonad a desktop as much as possible. Check them out if you like:

- Xmonad: https://www.youtube.com/playlist?list=PL5--8gKSku14_pucbTNI8__BcbekNifKV
- Rofi: https://www.youtube.com/watch?v=TutfIwxSE_s
- Nvim: https://www.youtube.com/playlist?list=PLhoH5vyxr6Qq41NFL4GvhFp-WLd5xzIzZ

## Updates

To Update the OS run the following in the terminal:

```
Update
```

This will update all packages installed and used by the OS.

## Suggestions

Any Suggestions? Feel free to open an issue!
