echo "#################################################################"
echo "# JIRR02OS IS STILL IN BETA. ARE YOU SURE YOU WANT TO CONTINUE? #"
echo "#################################################################"

read -p "(Y/N) \n" choice1

if [$choice1 = "N"]; then
    exit
fi

if [$choice1 = "n"]; then
    exit
fi

echo "########################"
echo "# UPDATING PACKAGES... #"
echo "########################"

sudo pacman -Syu --noconfirm --needed

echo "#########################"
echo "# INSTALLING PACKAGES...#"
echo "#########################"

sudo pacman -S --noconfirm --needed alacritty arandr blender discord flameshot freecad mixxx nitrogen obs-studio q-tractor rclone rofi signal-desktop thunderbird cura vlc vimfm whatsapp-for-linux python python-matplotlib hplip htop nodejs neovim picom thefuck xorg zip unzip autorandr blueman polybar xmonad xmonad-contrib base-devel zathura zathura-pdf-mupdf volumeicon curl

echo "#################################"
echo "# iNSTALLING AUR HELPER PARU,,, #"
echo "#################################"

git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si
cd

echo "#######################################"
echo "# INSTALLING PACKAGES FROM THE AUR... #"
echo "#######################################"

paru -S --noconfirm --needed balena-etcher brave foxitreader jabref openoffice-bin megasync realvnc-vnc-viewer whatsapp-for-linux texlive-full lightdm-webkit2-theme-arch

echo "########################"
echo "# PULLING CONFIG FILES #"
echo "########################"

mkdir JIrR02OS
git clone --bare https://gitlab.com/JirR02/Dotfiles.git
/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME pull

echo "#################################################"
echo "# INSTALLING OH MY ZSH & POWERLEVEL10K THEME... #"
echo "#################################################"

sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k