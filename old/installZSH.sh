# Use this shell script to move your aliases etc to your desired destination.

zsh/installzsh.sh
wait
cp zsh/zshrc /home/jirayu/.zshrc
cp zsh/alias.sh /home/jirayu/.alias.sh
cp zsh/bootquit.sh /home/jirayu/.bootquit-sh
cp zsh/git.sh /home/jirayu/.git.sh
cp zsh/rclone.sh /home/jirayu/.rclone.sh
cp zsh/shortcut.sh /home/jirayu/.shortcut.sh
zsh/installP10k.sh
wait
zsh/installZSHAutosuggestions.sh
wait
zsh/installZSHSyntaxHighliting.sh
wait
zsh/installOhMyZsh.sh
wait
