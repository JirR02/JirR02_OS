-- IMPORTS

-- Base
import XMonad
import System.Exit
import qualified XMonad.StackSet as W
import qualified Data.Map        as M

-- Data
import Data.Monoid

-- Hooks
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Hooks.ManageDocks (avoidStruts, docks, manageDocks,ToggleStruts(..))
import XMonad.Hooks.EwmhDesktops

-- Layout
import XMonad.Layout.Spacing
import XMonad.Layout.LayoutModifier
import XMonad.Layout.Renamed
import XMonad.Layout.WindowNavigation
import XMonad.Layout.SubLayouts

-- Layout Modifier
import XMonad.Layout.NoBorders
import XMonad.Layout.Simplest
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.ResizableTile
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL))

-- Utils
import XMonad.Util.SpawnOnce
import XMonad.Util.Run
import XMonad.Util.EZConfig

-------------------------------------------------------------------------

-- VARIABLES 

--Terminal
myTerminal :: String
myTerminal = "alacritty"

-- Whether focus follows the mouse pointer.
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

-- Whether clicking on a window to focus also passes the click to the window
myClickJustFocuses :: Bool
myClickJustFocuses = False

-- Width of the window border in pixels.
myBorderWidth :: Dimension
myBorderWidth = 2

-- ModKez
myModMask :: KeyMask
myModMask = mod4Mask

-- Border colors for unfocused and focused windows
myNormalBorderColor  = "#dddddd"
myFocusedBorderColor = "#3d85c6"

-- WindowsCount
windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

-- STARTUPHOOK
myStartupHook :: X ()
myStartupHook = do
    spawnOnce "nitrogen --restore &"
    spawnOnce "picom"
    
-------------------------------------------------------------------------

-- LAYOUTS

-- Sets spacing around Window
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

tall     = renamed [Replace "tall"]
           $ smartBorders
           $ windowNavigation
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 5
           $ mySpacing 8
           $ ResizableTall 1 (3/100) (1/2) []
monocle  = renamed [Replace "monocle"]
           $ smartBorders
           $ windowNavigation
           $ subLayout [] (smartBorders Simplest)
           $ Full

myLayoutHook = avoidStruts $ myDefaultLayout
            where
                myDefaultLayout = withBorder myBorderWidth tall
                                                       ||| monocle

---------------------------------------------------------------------------

-- WORKSPACES
myWorkspaces = [" cmd "," www "," doc "," pdf "," dev "," mod "," vid "," tlab "," chat "]

myManageHook :: XMonad.Query (Data.Monoid.Endo WindowSet)
myManageHook = composeAll
     -- 'doFloat' forces a window to float.  Useful for dialog boxes and such.
     -- using 'doShift ( myWorkspaces !! 7)' sends program to workspace 8!
     -- I'm doing it this way because otherwise I would have to write out the full
     -- name of my workspaces and the names would be very long if using clickable workspaces.
     [ className =? "confirm"         --> doFloat
     , className =? "file_progress"   --> doFloat
     , className =? "dialog"          --> doFloat
     , className =? "download"        --> doFloat
     , className =? "error"           --> doFloat
     , className =? "Gimp"            --> doFloat
     , className =? "notification"    --> doFloat
     , className =? "pinentry-gtk-2"  --> doFloat
     , className =? "splash"          --> doFloat
     , className =? "toolbar"         --> doFloat
     , title =? "Mozilla Firefox"     --> doShift ( myWorkspaces !! 3 )
     , (className =? "firefox" <&&> resource =? "Dialog") --> doFloat  -- Float Firefox Dialog
     ]

---------------------------------------------------------------------------

-- KEYBINDINGS
myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $

    -- launch a terminal
    [ ((modm .|. shiftMask, xK_Return), spawn $ XMonad.terminal conf)

    -- change Keyboard Layout
    , ((modm .|. shiftMask, xK_t     ), spawn "$HOME/.config/layout_switch.sh")

    -- Volume Keys
    , ((0,                     xK_F10), spawn "amixer set Master toggle")
    , ((0,                     xK_F11), spawn "amixer set Master 5%- unmute")
    , ((0,                     xK_F12), spawn "amixer set Master 5%+ unmute")

    -- Monitor Brightness
    , ((0,                     xK_F1), spawn "xbacklight -5%")
    , ((0,                     xK_F2), spawn "xbacklight +5%")

    -- Keyboard Backlight Brightness
    , ((0,                     xK_F5), spawn "xset led 1")
    , ((0,                     xK_F6), spawn "xset -led 1")

    -- Toggle Struts
    , ((modm,                  xK_f),  sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts)

    -- launch dmenu
    , ((modm,                  xK_p), spawn "dmenu_run")

    -- launch gmrun
    , ((modm .|. shiftMask,    xK_p), spawn "gmrun")

    -- close focused window
    , ((modm .|. shiftMask,    xK_c), kill)

     -- Rotate through the available layout algorithms
    , ((modm,                  xK_space), sendMessage NextLayout)

    --  Reset the layouts on the current workspace to default
    , ((modm .|. shiftMask,    xK_space), setLayout $ XMonad.layoutHook conf)

    -- Resize viewed windows to the correct size
    , ((modm,                  xK_n), refresh)

    -- Move focus to the next window
    , ((modm,                  xK_Tab), windows W.focusDown)

    -- Move focus to the next window
    , ((modm,                  xK_j), windows W.focusDown)

    -- Move focus to the previous window
    , ((modm,                  xK_k), windows W.focusUp  )

    -- Move focus to the master window
    , ((modm,                  xK_m), windows W.focusMaster  )

    -- Swap the focused window and the master window
    , ((modm,                  xK_Return), windows W.swapMaster)

    -- Swap the focused window with the next window
    , ((modm .|. shiftMask,    xK_j), windows W.swapDown  )

    -- Swap the focused window with the previous window
    , ((modm .|. shiftMask,    xK_k), windows W.swapUp    )

    -- Shrink the master area
    , ((modm,               xK_h     ), sendMessage Shrink)

    -- Expand the master area
    , ((modm,               xK_l     ), sendMessage Expand)

    -- Push window back into tiling
    , ((modm,               xK_t     ), withFocused $ windows . W.sink)

    -- Increment the number of windows in the master area
    , ((modm              , xK_comma ), sendMessage (IncMasterN 1))

    -- Deincrement the number of windows in the master area
    , ((modm              , xK_period), sendMessage (IncMasterN (-1)))

    -- Toggle the status bar gap
    -- Use this binding with avoidStruts from Hooks.ManageDocks.
    -- See also the statusBar function from Hooks.DynamicLog.
    --
    -- , ((modm              , xK_b     ), sendMessage ToggleStruts)

    -- Quit xmonad
    , ((modm .|. shiftMask, xK_q     ), io (exitWith ExitSuccess))

    -- Restart xmonad
    , ((modm              , xK_q     ), spawn "xmonad --recompile; xmonad --restart")

    -- Run xmessage with a summary of the default keybindings (useful for beginners)
    ]
    ++

    --
    -- mod-[1..9], Switch to workspace N
    -- mod-shift-[1..9], Move client to workspace N
    --
    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    ++

    --
    -- mod-{w,e,r}, Switch to physical/Xinerama screens 1, 2, or 3
    -- mod-shift-{w,e,r}, Move client to screen 1, 2, or 3
    --
    [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]


------------------------------------------------------------------------
-- Mouse bindings: default actions bound to mouse events
--
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    ]

------------------------------------------------------------------------

main :: IO ()
main = do
    xmproc1 <- spawnPipe "xmobar -x 0 /home/jirayuruh/.config/xmobar/.xmobarrc1"
    xmproc2 <- spawnPipe "xmobar -x 1 /home/jirayuruh/.config/xmobar/.xmobarrc2"
    xmproc3 <- spawnPipe "xmobar -x 2 /home/jirayuruh/.config/xmobar/.xmobarrc3"
    xmonad $ ewmh $ docks $ def{
      manageHook         = myManageHook <+> manageDocks
    , modMask            = myModMask
    , terminal           = myTerminal
    , startupHook        = myStartupHook
    , layoutHook         = myLayoutHook
    , keys               = myKeys
    , workspaces         = myWorkspaces
    , borderWidth        = myBorderWidth
    , normalBorderColor  = myNormalBorderColor
    , focusedBorderColor = myFocusedBorderColor
    , focusFollowsMouse  = myFocusFollowsMouse
    , clickJustFocuses   = myClickJustFocuses
    , mouseBindings      = myMouseBindings
    , logHook = dynamicLogWithPP $ xmobarPP
              -- XMOBAR SETTINGS
              { ppOutput = \x -> hPutStrLn xmproc1 x   -- xmobar on monitor 1
                              >> hPutStrLn xmproc2 x   -- xmobar on monitor 2
                              >> hPutStrLn xmproc3 x   -- xmobar on monitor 3
                -- Current workspace
              , ppCurrent = xmobarColor "#21c7a8" "" . wrap "[" "]"
                -- Visible but not current workspace
              , ppVisible = xmobarColor "#21c7a8" "" 
                -- Hidden workspace
              , ppHidden = xmobarColor "#c792ea" ""
                -- Hidden workspaces (no windows)
              , ppHiddenNoWindows = xmobarColor "#c792ea" ""
              -- Title of active window
              , ppTitle = xmobarColor "#ffffff" "" . shorten 30
              -- Separator character
              , ppSep =  " | "
                -- Urgent workspace
              , ppUrgent = xmobarColor "22da6de" "" . wrap "!" "!"
                -- Adding # of windows on current workspace to the bar
              , ppExtras  = [windowCount]
                -- order of things in xmobar
              , ppOrder  = \(ws:l:t:ex) -> [ws,l]++ex++[t]
              }
     }
